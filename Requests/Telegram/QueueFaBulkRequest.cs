namespace RadarsoftApi.Requests.Telegram
{
    public class QueueFaBulkRequest
    {
        public int[]? PostIds { get; set; }
    }
}
