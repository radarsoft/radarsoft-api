using System.Text.Json;

namespace RadarsoftApi.Requests.Telegram
{
    public class SendMessageRequest
    {
        public string? TelegramMessageType { get; set; }

        public string? ChatId { get; set; }

        public string? BaseContent { get; set; }

        public string? Caption { get; set; }

        public string? ParseMode { get; set; }

        public bool? DisableWebPagePreview { get; set; }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
