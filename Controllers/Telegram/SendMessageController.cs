using Microsoft.AspNetCore.Mvc;
using RadarsoftApi.Extensions;
using RadarsoftApi.Requests.Telegram;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace RadarsoftApi.Controllers.Telegram
{
    [ApiController]
    [Route("Telegram/[controller]")]
    public class SendMessageController : ControllerBase
    {
        private readonly ILogger<SendMessageController> _logger;
        private List<string>? AllowedBots { get; set; }

        public SendMessageController(ILogger<SendMessageController> logger)
        {
            _logger = logger;
            string? botTokens = Environment.GetEnvironmentVariable("TG_BOT_TOKENS");

            if (!string.IsNullOrEmpty(botTokens))
            {
                AllowedBots = botTokens.Split(';').ToList();
            }
        }

        private async Task<Stream> DownloadRemoteFile(string fileUrl)
        {
            using (HttpClient client = new())
            {
                return await client.GetStreamAsync(fileUrl);
            }
        }

        [HttpPost("")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status503ServiceUnavailable)]
        public async Task<ActionResult> SendMessage([FromHeader(Name = "X-Bot-Api-Token")]string xBotApiToken, [FromBody]SendMessageRequest requestBody)
        {
            HttpRequest currentRequest = Request;
            if (AllowedBots == null || AllowedBots.Count < 1)
            {
                return StatusCode(503, "No allowed bots.");
            }

            if (!AllowedBots.Contains(xBotApiToken))
            {
                return StatusCode(401, "Bot you are trying to use is not allowed.");
            }

            if(string.IsNullOrEmpty(requestBody.TelegramMessageType) || string.IsNullOrEmpty(requestBody.ChatId) || string.IsNullOrEmpty(requestBody.BaseContent))
            {
                return StatusCode(400, "One or more of the required params is null or empty");
            }
            
            string baseContentKey = requestBody.TelegramMessageType == "message" ? "text" : requestBody.TelegramMessageType;
            try
            {
                TelegramBotClient tgClient = new(new TelegramBotClientOptions(xBotApiToken, Environment.GetEnvironmentVariable("TG_API_URL")));
                ParseMode? parseMode = string.IsNullOrEmpty(requestBody.ParseMode)
                    ? null
                    : (ParseMode)Enum.Parse(typeof(ParseMode), requestBody.ParseMode.ToLower().FirstCharToUpper());
                Message telegramPostResult;
                telegramPostResult = baseContentKey switch
                {
                    "text" => await tgClient.SendTextMessageAsync(requestBody.ChatId, requestBody.BaseContent,
                        null, parseMode),
                    "photo" => await tgClient.SendPhotoAsync(requestBody.ChatId,
                        new InputFileStream(await DownloadRemoteFile(requestBody.BaseContent),
                            Path.GetFileName(new Uri(requestBody.BaseContent).LocalPath)), caption: requestBody.Caption,
                        parseMode: parseMode),
                    "document" => await tgClient.SendDocumentAsync(requestBody.ChatId,
                        new InputFileStream(await DownloadRemoteFile(requestBody.BaseContent),
                            Path.GetFileName(new Uri(requestBody.BaseContent).LocalPath)),
                        caption: requestBody.Caption, parseMode: parseMode),
                    "animation" => await tgClient.SendAnimationAsync(requestBody.ChatId,
                        new InputFileStream(await DownloadRemoteFile(requestBody.BaseContent),
                            Path.GetFileName(new Uri(requestBody.BaseContent).LocalPath)),
                        caption: requestBody.Caption, parseMode: parseMode),
                    _ => telegramPostResult = new Message()
                };

                return StatusCode(200, telegramPostResult);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "SendMessage failed with message: {Exception}", ex.Message);
                return StatusCode(500, ex.Message);
            }
        }
    }
}