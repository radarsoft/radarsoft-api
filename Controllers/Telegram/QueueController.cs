using System.Net.Http.Headers;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using RadarsoftApi.Requests.Telegram;
using RadarsoftApi.Responses.Hasura;

namespace RadarsoftApi.Controllers.Telegram
{
    [ApiController]
    [Route("Telegram/[controller]")]
    public class QueueController : ControllerBase
    {
        private readonly ILogger<QueueController> _logger;
        private List<string>? AllowedBots { get; }
        private string? GraphQlUrl { get; }
        private string? GraphQlAuth { get; }

        public QueueController(ILogger<QueueController> logger)
        {
            _logger = logger;
            string? botTokens = Environment.GetEnvironmentVariable("TG_BOT_TOKENS");
            GraphQlUrl = Environment.GetEnvironmentVariable("GQL_URL");
            GraphQlAuth = Environment.GetEnvironmentVariable("GQL_AUTH");
            

            if (!string.IsNullOrEmpty(botTokens))
            {
                AllowedBots = botTokens.Split(';').ToList();
            }
        }

        [EnableCors("Furaffinity")]
        [HttpPost("FA")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status503ServiceUnavailable)]
        public async Task<ActionResult> QueueFa([FromHeader(Name = "X-Bot-Api-Token")]string xBotApiToken, [FromBody]QueueFaRequest requestBody)
        {
            if (AllowedBots == null || AllowedBots.Count < 1)
            {
                return StatusCode(503, "No allowed bots.");
            }

            if (string.IsNullOrEmpty(GraphQlUrl) || string.IsNullOrEmpty(GraphQlAuth))
            {
                return StatusCode(503, "GraphQL env-vars not set.");
            }

            if (!AllowedBots.Contains(xBotApiToken))
            {
                return StatusCode(401, "Bot you are trying to use is not allowed.");
            }

            if(requestBody.PostId is null or <= 0)
            {
                return StatusCode(400, "PostID is null or empty");
            }

            try
            {
                using (HttpClient client = new())
                {
                    client.DefaultRequestHeaders.Add("X-Hasura-Admin-Secret", GraphQlAuth);
                    
                    var requestData = new Dictionary<string, string> 
                    {
                        {"query", $"mutation {{ insert_fa_to_scrape_one(object: {{ post_id: {requestBody.PostId} }}) {{ post_id }} }}"}
                    };

                    JsonContent requestContent = JsonContent.Create(requestData, new MediaTypeHeaderValue("application/json"));
                    
                    HttpResponseMessage hasuraResponse = await client.PostAsync(GraphQlUrl, requestContent);

                    string hasuraResponseT = await hasuraResponse.Content.ReadAsStringAsync();
                    _logger.LogInformation("{HasuraResponseT}", hasuraResponseT);
                }

                return StatusCode(200, $"Saved {requestBody.PostId} for processing when able.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "{Message}", ex.Message);
                return StatusCode(500, ex.Message);
            }
        }

        [EnableCors("Furaffinity")]
        [HttpPost("FA/Bulk")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status503ServiceUnavailable)]
        public async Task<ActionResult> QueueFaBulk([FromHeader(Name = "X-Bot-Api-Token")]string xBotApiToken, [FromBody]QueueFaBulkRequest requestBody)
        {
            if (AllowedBots == null || AllowedBots.Count < 1)
            {
                return StatusCode(503, "No allowed bots.");
            }

            if (string.IsNullOrEmpty(GraphQlUrl) || string.IsNullOrEmpty(GraphQlAuth))
            {
                return StatusCode(503, "GraphQL env-vars not set.");
            }

            if (!AllowedBots.Contains(xBotApiToken))
            {
                return StatusCode(401, "Bot you are trying to use is not allowed.");
            }

            if(requestBody.PostIds == null || !requestBody.PostIds.Any())
            {
                return StatusCode(400, "PostIDs cannot be null or empty!");
            }

            try
            {
                using (HttpClient client = new())
                {
                    client.DefaultRequestHeaders.Add("X-Hasura-Admin-Secret", GraphQlAuth);

                    string objectsToInsert = string.Join(", ", requestBody.PostIds.Select(id => $"{{ post_id: {id}, origin: \"FA\", origin_id_comb: \"FA_{id}\" }}"));

                    var requestData = new Dictionary<string, string>
                    {
                        {
                            "query",  $"mutation {{ insert_fa_to_scrape(objects: [ {objectsToInsert} ], on_conflict: {{ constraint: fa_to_scrape_origin_id_comb_key, update_columns: [] }}) {{ affected_rows }} }}"
                        }
                    };

                    JsonContent requestContent = JsonContent.Create(requestData, new MediaTypeHeaderValue("application/json"));

                    HttpResponseMessage hasuraResponse = await client.PostAsync(GraphQlUrl, requestContent);

                    string hasuraResponseT = await hasuraResponse.Content.ReadAsStringAsync();
                    _logger.LogInformation("{HasuraResponseT}", hasuraResponseT);

                    return new JsonResult(await hasuraResponse.Content.ReadFromJsonAsync<HasuraResponse>())
                    {
                        StatusCode = (int)hasuraResponse.StatusCode
                    };
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "{Message}", ex.Message);
                return StatusCode(500, ex.Message);
            }
        }
    }
}