using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using System.Text;

namespace RadarsoftApi.Controllers.Covid
{
    [Route("Covid/[controller]")]
    [ApiController]
    public class HospitalizationsController : ControllerBase
    {
        private readonly ILogger<HospitalizationsController> _logger;

        private static Task<HttpResponseMessage> GetHospitalizations()
        {
            DateTime date = DateTime.Now.AddDays(-10);
            string dateAfter = date.ToString("yyyy-MM-dd");
            string requestUrl = $"https://onemocneni-aktualne.mzcr.cz/api/v3/hospitalizace?page=1&datum%5Bafter%5D={dateAfter}&apiToken={Environment.GetEnvironmentVariable("MZCR_TOKEN")}";

            HttpClient client = new();
            return client.GetAsync(requestUrl);
        }

        public HospitalizationsController(ILogger<HospitalizationsController> logger)
        {
            _logger = logger;
        }

        [HttpGet("TXT")]
        [Produces("text/plain")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetHospitalizationstTxt()
        {
            try
            {
                HttpResponseMessage response = await GetHospitalizations();
                JsonElement responseJson = JsonDocument.Parse(await response.Content.ReadAsStringAsync()).RootElement;

                if (responseJson.Equals(null)) return StatusCode(500);
                JsonElement resultsArray = responseJson.GetProperty("hydra:member");
                if (resultsArray.GetArrayLength() <= 0) return StatusCode(500);
                StringBuilder result = new();
                DateTime date = DateTime.Now;
                int minusDays = 1;
                for (int i = resultsArray.GetArrayLength(); i > 0; i--)
                {
                    JsonElement currentResult = resultsArray[i - 1];
                    DateTime currentDate = date.AddDays(-minusDays);

                    string pacient_prvni_zaznam = currentResult.GetProperty("pacient_prvni_zaznam").GetRawText();
                    string kum_pacient_prvni_zaznam = currentResult.GetProperty("kum_pacient_prvni_zaznam").GetRawText();
                    string pocet_hosp = currentResult.GetProperty("pocet_hosp").GetRawText();
                    string stav_bez_priznaku = currentResult.GetProperty("stav_bez_priznaku").GetRawText();
                    string stav_lehky = currentResult.GetProperty("stav_lehky").GetRawText();
                    string stav_stredni = currentResult.GetProperty("stav_stredni").GetRawText();
                    string stav_tezky = currentResult.GetProperty("stav_tezky").GetRawText();
                    string jip = currentResult.GetProperty("jip").GetRawText();
                    string kyslik = currentResult.GetProperty("kyslik").GetRawText();
                    string hfno = currentResult.GetProperty("hfno").GetRawText();
                    string upv = currentResult.GetProperty("upv").GetRawText();
                    string ecmo = currentResult.GetProperty("ecmo").GetRawText();
                    string tezky_upv_ecmo = currentResult.GetProperty("tezky_upv_ecmo").GetRawText();
                    string umrti = currentResult.GetProperty("umrti").GetRawText();
                    string kum_umrti = currentResult.GetProperty("kum_umrti").GetRawText();

                    var currentStrings = new List<string>
                    {
                        "datum",
                        currentResult.GetProperty("datum").GetString() ?? currentDate.ToString("yyyy-MM-dd"),
                        "pacient_prvni_zaznam",
                        $"{pacient_prvni_zaznam}",
                        "kum_pacient_prvni_zaznam",
                        $"{kum_pacient_prvni_zaznam}",
                        "pocet_hosp",
                        $"{pocet_hosp}",
                        "stav_bez_priznaku",
                        $"{stav_bez_priznaku}",
                        "stav_lehky",
                        $"{stav_lehky}",
                        "stav_stredni",
                        $"{stav_stredni}",
                        "stav_tezky",
                        $"{stav_tezky}",
                        "jip",
                        $"{jip}",
                        "kyslik",
                        $"{kyslik}",
                        "hfno",
                        $"{hfno}",
                        "upv",
                        $"{upv}",
                        "ecmo",
                        $"{ecmo}",
                        "tezky_upv_ecmo",
                        $"{tezky_upv_ecmo}",
                        "umrti",
                        $"{umrti}",
                        "kum_umrti",
                        $"{kum_umrti}"
                    };
                    result.AppendJoin('\n', currentStrings);
                    result.AppendLine();
                    result.AppendLine();
                    minusDays++;
                }

                return Ok(result.ToString().Replace("null", "0"));

            } 
            catch (Exception ex)
            {
                _logger.LogError(ex, "{Message}", ex.Message);
                return StatusCode(500, ex.Message);
            }
        }
    }
}
