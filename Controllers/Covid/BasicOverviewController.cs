using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using RadarsoftApi.Responses.Covid;
using System.Text;

namespace RadarsoftApi.Controllers.Covid
{
    [Route("Covid/[controller]")]
    [ApiController]
    public class BasicOverviewController : ControllerBase
    {
        private readonly ILogger<BasicOverviewController> _logger;

        public BasicOverviewController(ILogger<BasicOverviewController> logger)
        {
            _logger = logger;
        }

        private static Task<HttpResponseMessage> GetBasicOverviewOfficialAsync(string date)
        {
            HttpClient client = new();
            return client.GetAsync($"https://onemocneni-aktualne.mzcr.cz/api/v3/zakladni-prehled/{date}?apiToken={Environment.GetEnvironmentVariable("MZCR_TOKEN")}");
        }

        /// <summary>
        /// Gets latest basic overview data from official API
        /// </summary>
        /// <returns>Basic Overview from official API</returns>
        /// <response code="200">Returns latest data</response>
        /// <response code="503">When an exception occurs</response>
        [HttpGet("JSON")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<BasicOverview>> GetBasicJsonOverview()
        {
            DateTime date = DateTime.Now;
            DateTime yesterdayDate = date.AddDays(-1);

            BasicOverview result = new()
            {
                Date = date.ToString("yyyy-MM-dd"),
                YesterdayDate = yesterdayDate.ToString("yyyy-MM-dd")
            };

            try
            {
                HttpResponseMessage response = await GetBasicOverviewOfficialAsync(result.Date);
                JsonElement responseJson = JsonDocument.Parse(await response.Content.ReadAsStringAsync()).RootElement;

                if (responseJson.Equals(null)) return StatusCode(500, "Invalid data return from MZCR.");

                result.PcrTestsSum = responseJson.GetProperty("provedene_testy_celkem").GetRawText().Replace("null", "0");
                result.PositiveSum = responseJson.GetProperty("potvrzene_pripady_celkem").GetRawText().Replace("null", "0");
                result.ActiveCases = responseJson.GetProperty("aktivni_pripady").GetRawText().Replace("null", "0");
                result.Healed = responseJson.GetProperty("vyleceni").GetRawText().Replace("null", "0");
                result.Deaths = responseJson.GetProperty("umrti").GetRawText().Replace("null", "0");
                result.InHospital = responseJson.GetProperty("aktualne_hospitalizovani").GetRawText().Replace("null", "0");
                result.YesterdayPcrTests = responseJson.GetProperty("provedene_testy_vcerejsi_den").GetRawText().Replace("null", "0");
                result.YesterdayPositive = responseJson.GetProperty("potvrzene_pripady_vcerejsi_den").GetRawText().Replace("null", "0");
                result.AgTestsSum = responseJson.GetProperty("provedene_antigenni_testy_celkem").GetRawText().Replace("null", "0");
                result.YesterdayAgTests = responseJson.GetProperty("provedene_antigenni_testy_vcerejsi_den").GetRawText().Replace("null", "0");
                result.VaccinationsSum = responseJson.GetProperty("vykazana_ockovani_celkem").GetRawText().Replace("null", "0");
                result.YesterdayVaccinations = responseJson.GetProperty("vykazana_ockovani_vcerejsi_den").GetRawText().Replace("null", "0");
                result.Positive65Sum = responseJson.GetProperty("potvrzene_pripady_65_celkem").GetRawText().Replace("null", "0");
                result.YesterdayPositive65 = responseJson.GetProperty("potvrzene_pripady_65_vcerejsi_den").GetRawText().Replace("null", "0");
                result.VaccinatedSum = responseJson.GetProperty("ockovane_osoby_celkem").GetRawText().Replace("null", "0");
                result.YesterdayVaccinated = responseJson.GetProperty("ockovane_osoby_vcerejsi_den").GetRawText().Replace("null", "0");

                return Ok(result);

            } 
            catch (Exception ex)
            {
                _logger.LogError(ex, "{Message}", ex.Message);
                return StatusCode(500, ex.Message);
            }
            
        }

        /// <summary>
        /// Gets latest basic overview data from official API
        /// </summary>
        /// <returns>Basic Overview from official API</returns>
        /// <response code="200">Returns latest data</response>
        /// <response code="503">When an exception occurs</response>
        [HttpGet("TXT")]
        [Produces("text/plain")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<string>> GetBasicTxtOverview()
        {
            DateTime date = DateTime.Now;
            DateTime yesterdayDate = date.AddDays(-1);

            try
            {
                HttpResponseMessage response = await GetBasicOverviewOfficialAsync(date.ToString("yyyy-MM-dd"));
                JsonElement responseJson = JsonDocument.Parse(await response.Content.ReadAsStringAsync()).RootElement;

                if (responseJson.Equals(null)) return StatusCode(500, "Invalid data returned from MZCR");
                string provedene_testy_celkem = responseJson.GetProperty("provedene_testy_celkem").GetRawText();
                string potvrzene_pripady_celkem = responseJson.GetProperty("potvrzene_pripady_celkem").GetRawText();
                string aktivni_pripady = responseJson.GetProperty("aktivni_pripady").GetRawText();
                string vyleceni = responseJson.GetProperty("vyleceni").GetRawText();
                string umrti = responseJson.GetProperty("umrti").GetRawText();
                string aktualne_hospitalizovani = responseJson.GetProperty("aktualne_hospitalizovani").GetRawText();
                string provedene_testy_vcerejsi_den = responseJson.GetProperty("provedene_testy_vcerejsi_den").GetRawText();
                string potvrzene_pripady_vcerejsi_den = responseJson.GetProperty("potvrzene_pripady_vcerejsi_den").GetRawText();
                string provedene_antigenni_testy_celkem = responseJson.GetProperty("provedene_antigenni_testy_celkem").GetRawText();
                string provedene_antigenni_testy_vcerejsi_den = responseJson.GetProperty("provedene_antigenni_testy_vcerejsi_den").GetRawText();
                string vykazana_ockovani_celkem = responseJson.GetProperty("vykazana_ockovani_celkem").GetRawText();
                string vykazana_ockovani_vcerejsi_den = responseJson.GetProperty("vykazana_ockovani_vcerejsi_den").GetRawText();
                string potvrzene_pripady_65_celkem = responseJson.GetProperty("potvrzene_pripady_65_celkem").GetRawText();
                string potvrzene_pripady_65_vcerejsi_den = responseJson.GetProperty("potvrzene_pripady_65_vcerejsi_den").GetRawText();
                string ockovane_osoby_celkem = responseJson.GetProperty("ockovane_osoby_celkem").GetRawText();
                string ockovane_osoby_vcerejsi_den = responseJson.GetProperty("ockovane_osoby_vcerejsi_den").GetRawText();

                var arrayToAppend = new List<string>
                {
                    "datum",
                    date.ToString("yyyy-MM-dd"),
                    "vcerejsi_datum",
                    yesterdayDate.ToString("yyyy-MM-dd"),
                    "provedene_testy_celkem",
                    $"{provedene_testy_celkem}",
                    "potvrzene_pripady_celkem",
                    $"{potvrzene_pripady_celkem}",
                    "aktivni_pripady",
                    $"{aktivni_pripady}",
                    "vyleceni",
                    $"{vyleceni}",
                    "umrti",
                    $"{umrti}",
                    "aktualne_hospitalizovani",
                    $"{aktualne_hospitalizovani}",
                    "provedene_testy_vcerejsi_den",
                    $"{provedene_testy_vcerejsi_den}",
                    "potvrzene_pripady_vcerejsi_den",
                    $"{potvrzene_pripady_vcerejsi_den}",
                    "provedene_antigenni_testy_celkem",
                    $"{provedene_antigenni_testy_celkem}",
                    "provedene_antigenni_testy_vcerejsi_den",
                    $"{provedene_antigenni_testy_vcerejsi_den}",
                    "vykazana_ockovani_celkem",
                    $"{vykazana_ockovani_celkem}",
                    "vykazana_ockovani_vcerejsi_den",
                    $"{vykazana_ockovani_vcerejsi_den}",
                    "potvrzene_pripady_65_celkem",
                    $"{potvrzene_pripady_65_celkem}",
                    "potvrzene_pripady_65_vcerejsi_den",
                    $"{potvrzene_pripady_65_vcerejsi_den}",
                    "ockovane_osoby_celkem",
                    $"{ockovane_osoby_celkem}",
                    "ockovane_osoby_vcerejsi_den",
                    $"{ockovane_osoby_vcerejsi_den}"
                };
                StringBuilder resultBuilder = new();
                resultBuilder.AppendJoin('\n', arrayToAppend);

                return Ok(resultBuilder.ToString().Replace("null", "0"));

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "{Message}", ex.Message);
                return StatusCode(500, ex.Message);
            }
        }
    }
}
