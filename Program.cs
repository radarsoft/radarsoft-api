WebApplicationBuilder builder = WebApplication.CreateBuilder(new WebApplicationOptions
{
    Args = args,
    ContentRootPath = "/app/"
});

string? version = Environment.GetEnvironmentVariable("API_VERSION");

// Add services to the container.
builder.Services.AddCors(options =>
{
    options.AddPolicy("Furaffinity", policy =>
    {
        policy
            .WithOrigins("https://www.furaffinity.net", "https://furaffinity.net")
            .WithMethods("POST", "OPTIONS")
            .WithHeaders("X-Bot-Api-Token", "Content-Type");
    });
});
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options => {
    options.SwaggerDoc($"v{version}", new Microsoft.OpenApi.Models.OpenApiInfo{
        Version = version,
        Title = "Radarsoft API"
    });
});
builder.Services.AddHealthChecks();
builder.Services.AddLogging();
builder.Services.AddHttpsRedirection(_ => { });
builder.Services.AddHttpLogging(_ => { });

builder.WebHost.ConfigureKestrel(kestrel =>
{
    kestrel.AddServerHeader = false;
    kestrel.ListenAnyIP(80);
});

WebApplication app = builder.Build();
app.UseHealthChecks("/AppHealth");
app.UseHttpsRedirection();
app.UseCors();
app.UseHttpLogging();

app.UseSwagger();
app.UseSwaggerUI(options => {
    options.SwaggerEndpoint($"/swagger/v{version}/swagger.json", "Radarsoft API");
});

app.MapControllers();
app.Run();
