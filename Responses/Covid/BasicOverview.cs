namespace RadarsoftApi.Responses.Covid
{
    public class BasicOverview
    {
        /// <summary>
        /// Date for the numbers
        /// </summary>
        public string? Date { get; set; }

        /// <summary>
        /// Date for the numbers prefixed 'Yesterday'
        /// </summary>
        public string? YesterdayDate { get; set; }

        /// <summary>
        /// Number of PCR tests since the start of monitoring
        /// </summary>
        public string? PcrTestsSum { get; set; }

        /// <summary>
        /// Number of positive cases since the start of monitoring
        /// </summary>
        public string? PositiveSum { get; set; }

        /// <summary>
        /// Number of currently active cases
        /// </summary>
        public string? ActiveCases { get; set; }

        /// <summary>
        /// Number of people recovered from the disease since the start of monitoring
        /// </summary>
        public string? Healed { get; set; }

        /// <summary>
        /// Number of people dying because of the disease since the start of monitoring
        /// </summary>
        public string? Deaths { get; set; }

        /// <summary>
        /// Number of people currently hospitalized.
        /// </summary>
        public string? InHospital { get; set; }

        /// <summary>
        /// Number of PCR tests done previous day
        /// </summary>
        public string? YesterdayPcrTests { get; set; }

        /// <summary>
        /// Number of positive cases for the previous day
        /// </summary>
        public string? YesterdayPositive { get; set; }

        /// <summary>
        /// Number of quick tests since the start of monitoring
        /// </summary>
        public string? AgTestsSum { get; set; }

        /// <summary>
        /// Number of quick tests done on the previous day
        /// </summary>
        public string? YesterdayAgTests { get; set; }

        /// <summary>
        /// Number of issued vaccination doses since the start of monitoring
        /// </summary>
        public string? VaccinationsSum { get; set; }

        /// <summary>
        /// Number of issued vaccination doses for the previous day
        /// </summary>
        public string? YesterdayVaccinations { get; set; }

        /// <summary>
        /// Number of positive cases among people of age 65 and higher since the start of monitoring
        /// </summary>
        public string? Positive65Sum { get; set; }

        /// <summary>
        /// Number of positive cases among people of age 65 and higher for the previous day
        /// </summary>
        public string? YesterdayPositive65 { get; set; }

        /// <summary>
        /// Number of people vaccinated since the start of monitoring
        /// </summary>
        public string? VaccinatedSum { get; set; }

        /// <summary>
        /// Number of people vaccinated on the previous day only
        /// </summary>
        public string? YesterdayVaccinated { get; set; }
    }
}
