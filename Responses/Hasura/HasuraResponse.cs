namespace RadarsoftApi.Responses.Hasura
{
#pragma warning disable IDE1006 // Naming Styles
    public class HasuraResponse 
    {
        public object? data { get; set; }
        public object? errors { get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}