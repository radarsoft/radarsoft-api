FROM mcr.microsoft.com/dotnet/aspnet:8.0.0-alpine3.18-arm64v8 AS base
WORKDIR /app
EXPOSE 8080
FROM mcr.microsoft.com/dotnet/sdk:8.0.100-1-alpine3.18 AS build
WORKDIR /src
COPY ["radarsoft-api.csproj", "./"]
RUN dotnet restore "./radarsoft-api.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "radarsoft-api.csproj" -c Release -o /app/build
FROM build AS publish
RUN dotnet publish "radarsoft-api.csproj" -c Release -o /app/publish -r linux-arm64 --no-self-contained
FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "radarsoft-api.dll"]
